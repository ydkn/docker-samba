# base image
FROM ubuntu:rolling

# args
ARG S6_OVERLAY_VERSION=3.1.6.2
ARG TARGETARCH
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/samba" \
  org.label-schema.description="Simple Samba docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/samba" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/docker-samba" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apt-get update && apt-get install -y --no-install-recommends xz-utils tzdata curl ca-certificates krb5-user acl attr \
  smbclient samba samba-dsdb-modules samba-vfs-modules winbind libnss-winbind libpam-winbind && rm -rf /var/lib/apt/lists/*

# install s6-overlay
RUN case ${TARGETARCH} in \
      "amd64") S6_ARCH=x86_64 ;; \
      "arm64") S6_ARCH=aarch64 ;; \
    esac \
  && curl https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz -L -s --output /tmp/s6-overlay-noarch.tar.xz \
  && curl https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_ARCH}.tar.xz -L -s --output /tmp/s6-overlay-${S6_ARCH}.tar.xz \
  && tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz \
  && tar -C / -Jxpf /tmp/s6-overlay-${S6_ARCH}.tar.xz

# copy files
COPY s6-rc.d/ /etc/s6-overlay/s6-rc.d/
COPY scripts/ /etc/s6-overlay/scripts/

# enable services
RUN bash -c "touch /etc/s6-overlay/s6-rc.d/user/contents.d/{nmbd,smbd,winbindd}"

ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME="0"

# ports
EXPOSE 137/udp 138/udp 139/tcp 445/tcp

# healthcheck
HEALTHCHECK --interval=60s --timeout=15s \
  CMD smbclient -L \\localhost -U % -m SMB3

# volumes
VOLUME ["/etc", "/var/cache/samba", "/var/lib/samba", "/var/log/samba", "/run/samba"]

# entrypoint
ENTRYPOINT [ "/init" ]
