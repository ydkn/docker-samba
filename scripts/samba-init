#!/command/with-contenv sh

JOIN_SECRET_PATH="/run/secrets/samba-dc-join-credentials"

if [ ! -d /var/lib/samba/private ]; then
  mkdir -p /var/lib/samba/private
fi

if [ ! -w "/etc/samba/smb.conf" ]; then
  echo "Cannot write to /etc/samba/smb.conf. Skipping init. Please mount smb.conf to /config/smb.conf instead of /etc/samba/smb.conf."

  exit 0
fi

if [ ! -f "/config/smb.conf" ]; then
  echo "/config/smb.conf not found. Skipping init."

  exit 0
fi

cat /config/smb.conf >/etc/samba/smb.conf

if [ -f /var/lib/samba/private/passdb.tdb ]; then
  echo "Already initialized. Skipping init."

  exit 0
fi

if [ -n "${REALM}" ]; then
  if [ ! -f "${JOIN_SECRET_PATH}/username" ] || [ ! -f "${JOIN_SECRET_PATH}/password" ]; then
    echo "${JOIN_SECRET_PATH}/{username,password} does not exist. Not joining domain."

    exit 0
  fi

  samba-tool domain join "${REALM}" MEMBER \
    --username="$(cat "${JOIN_SECRET_PATH}/username")" \
    --password="$(cat "${JOIN_SECRET_PATH}/password")" \
    --realm="${REALM}" \
    --workgroup="${WORKGROUP}" \
    --no-dns-updates

  echo "Successfully joined domain ${REALM}. Restarting..."

  cat /config/smb.conf >/etc/samba/smb.conf
fi

echo "Initialized."

exit 0
