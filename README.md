# Samba Docker Image

## Configuration

Create a _smb.conf_ file and mount it to _/config/smb.conf_.

## Start the container

```bash
docker run -d --restart always \
  -v $(pwd)/smb.conf:/config/smb.conf \
  -v $(pwd)/share:/share \
  -v $(pwd)/samba:/var/lib/samba \
  ydkn/samba:latest
```
